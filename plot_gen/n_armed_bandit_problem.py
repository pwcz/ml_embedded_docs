#!/usr/bin/env python3
import numpy as np
# from scipy import stats
import random
import matplotlib.pyplot as plt


class NArmedBanditProblem:
    def __init__(self):
        self.n = 10
        self.arms = np.random.rand(self.n)
        self.eps = 0.5

    def reward(self, prob):
        """
        Function for calculation one arm bandit reward
        :param prob: gets probability of arm
        :return: reward taken from one arm bandit
        """
        reward = 0
        for i in range(10):
            if random.random() < prob:
                reward += 1
        return reward

    def bestArm(self, a):
        """
        greedy method to select best arm based on memory array (historical results)
        :param a: history of selected bandits and rewards
        :return: best arm from history
        """
        bestArm = 0  # just default to 0
        bestMean = 0
        for u in a:
            avg = np.mean(a[np.where(a[:,0] == u[0])][:, 1])  # calc mean reward for each action
            if bestMean < avg:
                bestMean = avg
                bestArm = u[0]
        return bestArm

    def first_implementation_with_big_memorry(self, eps=0.5):
        self.eps = eps
        # initialize memory array; has 1 row defaulted to random action index
        x, y = [], []
        av = np.array([np.random.randint(0, (self.n+1)), 0]).reshape(1, 2) #av = action-value
        for i in range(500):
            if random.random() > self.eps:  # greedy arm selection
                choice = self.bestArm(av)
                thisAV = np.array([[choice, self.reward(self.arms[choice])]])
                av = np.concatenate((av, thisAV), axis=0)  # Join a sequence of arrays along an existing axis

            else:  # random arm selection
                choice = np.where(self.arms == np.random.choice(self.arms))[0][0]
                thisAV = np.array([[choice, self.reward(self.arms[choice])]])  # choice, reward
                av = np.concatenate((av, thisAV), axis=0)  # add to our action-value memory array
            # calculate the percentage the correct arm is chosen (you can plot this instead of reward)
            # percCorrect = 100*(len(av[np.where(av[:, 0] == np.argmax(arms))])/len(av))
            # calculate the mean reward
            runningMean = np.mean(av[:, 1])
            plt.scatter(i, runningMean)
            x.append(i)
            y.append(runningMean)
        return x, y

    def second_implemetation_with_avarage_memorry(self):
        x, y = [], []
        # initialize action-value array
        av = np.ones(self.n)
        # stores counts of how many times we've taken a particular action
        counts = np.zeros(self.n)
        result = []

        def bestArm(a):
            # returns index of element with greatest value
            return np.argmax(a)
        plt.xlabel("Plays")
        plt.ylabel("Mean Reward")
        for i in range(500):
            if random.random() > self.eps:
                choice = bestArm(av)
                counts[choice] += 1
                k = counts[choice]
                rwd = self.reward(self.arms[choice])
                old_avg = av[choice]
                new_avg = old_avg + (1/k)*(rwd - old_avg)  # update running avg
                av[choice] = new_avg
            else:
                # randomly choose an arm (returns index)
                choice = np.where(self.arms == np.random.choice(self.arms))[0][0]
                counts[choice] += 1
                k = counts[choice]
                rwd = self.reward(self.arms[choice])
                old_avg = av[choice]
                # update running avg
                new_avg = old_avg + (1/k)*(rwd - old_avg)
                av[choice] = new_avg
            # have to use np.average and supply the weights to get a weighted average
            runningMean = np.average(av, weights=np.array([counts[i]/np.sum(counts) for i in range(len(counts))]))
            x.append(i)
            y.append(runningMean)
        return x, y

    def third_implemetation_with_softmax(self):
        x, y = [], []
        n = self.n
        arms = self.arms

        av = np.ones(n)  # initialize action-value array, stores running reward mean
        counts = np.zeros(n)  # stores counts of how many times we've taken a particular action
        # stores our softmax-generated probability ranks for each action
        av_softmax = np.zeros(n)
        av_softmax[:] = 0.1  # initialize each action to have equal probability

        def reward(prob):
            total = 0
            for i in range(10):
                if random.random() < prob:
                    total += 1
            return total

        tau = 1.12  # tau was selected by trial and error

        def softmax(av):
            probs = np.zeros(n)
            for i in range(n):
                softm = (np.exp(av[i] / tau) / np.sum(np.exp(av[:] / tau)))
                probs[i] = softm
            return probs

        for i in range(500):
            # select random arm using weighted probability distribution
            choice = np.where(arms == np.random.choice(arms, p=av_softmax))[0][0]
            counts[choice] += 1
            k = counts[choice]
            rwd = reward(arms[choice])
            old_avg = av[choice]
            new_avg = old_avg + (1/k)*(rwd - old_avg)
            av[choice] = new_avg
            av_softmax = softmax(av)  # update softmax probabilities for next play
            runningMean = np.average(av, weights=np.array([counts[i]/np.sum(counts) for i in range(len(counts))]))
            x.append(i)
            y.append(runningMean)
        return x, y

if __name__ == "__main__":
    main = NArmedBanditProblem()
    """
    x1, y1 = main.first_implementation_with_big_memorry()
    x2, y2 = main.second_implemetation_with_avarage_memorry()
    x3, y3 = main.third_implemetation_with_softmax()

    plt.xlabel("Plays")
    plt.ylabel("Avg Reward")
    plt.plot(x1, y1, marker="s", label='first', color='red')
    plt.plot(x2, y2, marker="d", label="second", color='blue')
    plt.plot(x3, y3, marker="o", label="third", color='yellow')
    plt.legend(['first', 'second', 'softmax'])
    plt.show()
    """
    plt.xlabel("Plays")
    plt.ylabel("Avg Reward")
    legend = []
    markers = [".", ",", "o", "v", "1", "8", "s", "p", "*", "h"]
    for j in range(0, 10, 1):
        eps = float(j)/10
        x, y = main.first_implementation_with_big_memorry(eps=eps)
        plt.plot(x, y, marker=markers[j])
        legend.append(str(eps))
    plt.legend(legend)
    plt.show()
